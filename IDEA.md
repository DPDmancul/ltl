# Lua Typesetting Language

A typesetting language highly inspired by TeX & Co. which aims to target multiple formats (PDF, HTML, ePub, ...) using Lua as scripting language.

## TeX-like "macros"

Essentially a ltl file is a literate lua file: the main mode is ltl mode (textual mode). To access lua functionalities you can use:

- `\macro#1#2#3...` will call lua function `macro(#1, #2, #3, ...)`
- `\variable` will display the lua variable
- `\variable=value` will set the local variable
- `\(args) body \end` will define a lambda
- `\code;` executes a lua instruction
- `\lua code \end` executes lua code
- `\do ltl \end` inner scope

To print from lua you can use:
- `ltl.print` function
- `\@macro#1#2` ...
- `\@code;`

## Example

```tex
\ltl.meta.title=Example % Equivalent of \ltl.meta.title="Example";
\ltl.meta.author={John Smith}

\ltl.fontsize=12pt

\ltl.engines={
  require "ltl-luatex", % Enbale output of luatex engine
  require "ltl-epub", % Enable output of ePub engine
};

\if{\engine.is_mode{pdf}} % If we are in pdf mode (e.g with luatex engine)
  \pdf=\require{ltl-pdf} % Equivalent of \pdf=require"ltl-pdf";
  \pdf.papersize=\pdf.papersizes.a4 % set pdf page size as a4 (Default)
\end

\ltl.doc\(engine)
  \engine.maketitle
\end
```
